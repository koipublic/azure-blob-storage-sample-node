"use strict";

require("dotenv").config();

const logger = require("./pinoLogger");
const uuidv1 = require("uuid/v1");

const { BlobServiceClient } = require("@azure/storage-blob");

function pad2(n) { 
  return n < 10 ? "0" + n : n;
}

function get_formatted_time() {
  const date = new Date();
  const formattedTime = date.getFullYear().toString() + pad2(date.getMonth() + 1) + pad2( date.getDate()) + pad2( date.getHours() ) + pad2( date.getMinutes() ) + pad2( date.getSeconds() );
  logger.info(formattedTime)
  return formattedTime;
}

// A helper function used to read a Node.js readable stream into a string
async function streamToString(readableStream) {
  return new Promise((resolve, reject) => {
    const chunks = [];
    readableStream.on("data", (data) => {
      chunks.push(data.toString());
    });
    readableStream.on("end", () => {
      resolve(chunks.join(""));
    });
    readableStream.on("error", reject);
  });
}

async function list_all_blobs(containerClient) {
    logger.info("\nListing blobs...");

    // List the blob(s) in the container.
    for await (const blob of containerClient.listBlobsFlat()) {
        logger.info("\t", blob.name);
    }
}

async function create_blob_container() {
    // Create the BlobServiceClient object which will be used to create a container client
    const blobServiceClient = await BlobServiceClient.fromConnectionString(process.env.AZURE_STORAGE_CONNECTION_STRING);

    const BLOB_STORAGE_CONTAINER_NAME = "nano-dev";
    // logger.info("\nCreating container...");
    // const BLOB_STORAGE_CONTAINER_NAME = `nano-dev-${get_formatted_time()}`;
    
    logger.info("\t", BLOB_STORAGE_CONTAINER_NAME);

    // Get a reference to the container
    const containerClient = await blobServiceClient.getContainerClient(BLOB_STORAGE_CONTAINER_NAME);

    // Create the container
    // const createContainerResponse = await containerClient.create();
    // logger.info("Container was created successfully. requestId: ", createContainerResponse.requestId);

    return containerClient;
}

async function upload_to_blob_storage(containerClient, blobName) {
    
    // Get a block blob client
    const blockBlobClient = containerClient.getBlockBlobClient(blobName);

    logger.info("\nUploading to Azure storage as blob:\n\t", blobName);

    // Upload data to the blob
    const data = "Hello, World!";
    const uploadBlobResponse = await blockBlobClient.upload(data, Buffer.byteLength(data));
    logger.info("Blob was uploaded successfully. requestId: ", uploadBlobResponse.requestId);
}

async function main() {
    logger.info("Azure Blob storage v12");

    const containerClient = await create_blob_container();

    await list_all_blobs(containerClient);

    // Create a unique name for the blob
    const blobName = `${uuidv1()}.txt`;
    await upload_to_blob_storage(containerClient, blobName);
}

main().then(() => logger.info("Done")).catch((err) => logger.error(err.message));
