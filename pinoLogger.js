"use strict";

const pino = require("pino");
let logger;

if (process.env.NODE_ENV !== "production") {
  const pinoLogger = pino({
    prettyPrint: true,
    colorize: true,
    level: "info",
    timestamp: pino.stdTimeFunctions.isoTime,
    enabled: true
  });
  logger = require("pino-caller")(pinoLogger); // Don't use in production. It will print also the calling site
} else {
  logger = pino({
    prettyPrint: true,
    colorize: true,
    level: "info", // Warn
    timestamp: pino.stdTimeFunctions.isoTime,
    enabled: true
  });
}

module.exports = logger;
